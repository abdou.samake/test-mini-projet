import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import * as express from "express";
import * as cors from "cors"
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import {cert} from "./cred/cert";
import {ShoesModel} from "./model/shoes.model";

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://mini-projet-b1aee.firebaseio.com"
});
const app = express()
app.use(cors({origin: true}))
const db = admin.firestore();
const refShoes = db.collection('shoes');

async function testShoeExistById(shoeId: string) {
    const refShoe = refShoes.doc(shoeId);
    const snapShotUserRef: DocumentSnapshot = await refShoe.get();
    const shoeToFind: ShoesModel = snapShotUserRef.data() as ShoesModel;
    if(!shoeToFind) {
        throw new Error('player does note exist');
    }
    return refShoe;
}

export async function deleteShoeById(shoeId: string): Promise<string> {
    const shoeToDelete = await testShoeExistById(shoeId);
    await shoeToDelete.delete();
    return ` shoe id: ${shoeToDelete.id} is delete `;
}

app.post('/', async (req, res) => {
    try {
        const shoeId = req.body.id;
        const result = await deleteShoeById(shoeId);
        return res.send(result)
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }

})
 export const do_delete_shoe = functions
     .region("europe-west1")
     .runWith({memory: "128MB"})
     .https
     .onRequest(app);
