import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import * as express from "express";
import * as cors from "cors"
import {cert} from "./cred/cert";
import {ShoesModel} from "./model/shoes.model";

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://mini-projet-b1aee.firebaseio.com"
});
const app = express()
app.use(cors({origin: true}))
const db = admin.firestore();
const refShoes = db.collection('shoes');

async function addShoes(newShoe: ShoesModel): Promise<ShoesModel> {
    if(!newShoe) {
        throw new Error('newShoe does not exist')
    }
    const addShoe = await refShoes.add(newShoe);
    return ({...newShoe, id: addShoe.id})
}

app.post('/', async (req, res) => {
    try {
        const newShoe = req.body;
        const addResult = await addShoes(newShoe);
        return res.send(addResult)
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }

})
 export const do_create_shoe = functions
     .region("europe-west2")
     .runWith({memory: "128MB"})
     .https
     .onRequest(app)
