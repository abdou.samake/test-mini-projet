import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
import * as express from "express";
import * as cors from "cors"
import {ShoesModel} from "./model/shoes.model";

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://mini-projet-b1aee.firebaseio.com"
});

const app = express()
app.use(cors({origin: true}))
const db = admin.firestore();
const refShoes = db.collection('shoes');

export async function getAllShoes(): Promise<ShoesModel[]> {
    const snapRefShoes = await refShoes.get();
    const shoes: ShoesModel[] = [];
    snapRefShoes.forEach((snapRefShoe) => shoes.push(snapRefShoe.data() as ShoesModel));
    return shoes;
}

app.post('/', async (req, res) => {
    try {
        const users = await getAllShoes();
        return res.send(users);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
})
 export const do_read_shoe = functions
     .region("europe-west2")
     .runWith({memory: "128MB"})
     .https
     .onRequest(app)
