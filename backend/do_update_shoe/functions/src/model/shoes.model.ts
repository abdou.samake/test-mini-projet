export interface ShoesModel {
    name: string;
    price: string;
    quality: string;
    id: string;
}