import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import * as express from "express";
import * as cors from "cors"
import {cert} from "./cred/cert";
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import {ShoesModel} from "./model/shoes.model";

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://mini-projet-b1aee.firebaseio.com"
});

const app = express()
app.use(cors({origin: true}))
const db = admin.firestore();
const refShoes = db.collection('shoes');

async function testShoeExistById(shoeId: string) {
    const refShoe = refShoes.doc(shoeId);
    const snapShotUserRef: DocumentSnapshot = await refShoe.get();
    const shoeToFind: ShoesModel = snapShotUserRef.data() as ShoesModel;
    if(!shoeToFind) {
        throw new Error('player does note exist');
    }
    return refShoe;
}
export async function putShoeById(shoeId: string, newShoe: ShoesModel) {
    if (!shoeId || !newShoe) {
        throw new Error('playerId or newPlayer are not exists');
    }
    const shoeToPatch = await testShoeExistById(shoeId);
    return await shoeToPatch.set(newShoe);
}
export async function patchShoeById(shoeId: any, newShoe: ShoesModel) {
    if (!shoeId || !newShoe) {
        throw new Error('playerId or newPlayer are not exists');
    }
    const shoeToPatch = await testShoeExistById(shoeId);
    return await shoeToPatch.update(newShoe);
}

app.post('/', async (req, res) => {
    try {
        const shoeId = req.body.id;
        const newShoe = req.body;
        const addResult = await patchShoeById(shoeId, newShoe);
        return res.send(addResult)
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }

})
 export const do_update_shoe = functions
     .region("europe-west1")
     .runWith({memory: "128MB"})
     .https
     .onRequest(app);